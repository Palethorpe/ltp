// SPDX-License-Identifier: GPL-2.0-or-later
/*
 * Copyright (c) 2020 FUJITSU LIMITED. All rights reserved.
 * Author: Yang Xu <xuyang2018.jy@cn.fujitsu.com>
 *
 * Before kernel 2.6.26, we can't trace init(1) process and ptrace() will
 * get EPERM error. This case just check whether we can trace init(1)
 * process and doesn't trigger error.
 */

#include <errno.h>
#include <signal.h>
#include <sys/wait.h>
#include <pwd.h>
#include <config.h>
#include <stdlib.h>
#include "ptrace.h"
#include "tst_test.h"

static void verify_ptrace(void)
{
	TEST(ptrace(PTRACE_ATTACH, 1, NULL, NULL));
	if (TST_RET == 0)
		tst_res(TPASS, "ptrace() traces init process successfully");
	else
		tst_res(TFAIL | TTERRNO,
			"ptrace() returns %ld, failed unexpectedly", TST_RET);

	/*
	 * Running attach/detach more times will trigger a ESRCH error because
	 * ptrace_check_attach function in kernel will report it if its process
	 * stats is not __TASK_TRACED.
	 */
	TST_RETRY_FUNC(ptrace(PTRACE_DETACH, 1, NULL, NULL), TST_RETVAL_EQ0);
}

static struct tst_test test = {
	.test_all = verify_ptrace,
	.needs_root = 1,
};
